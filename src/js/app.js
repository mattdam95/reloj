const reloj = document.querySelector(".watch");

eventListener();

function eventListener() {
    document.addEventListener("DOMContentLoaded", () => {
        watch();
    });
}

const semana = [
    "Lunes",
    "Martes",
    "Miércoles",
    "Jueves",
    "Viernes",
    "Sábado",
    "Domingo",
];

function watch() {
    const days = document.querySelector(".dias");
    const horas = document.querySelector(".horas");
    const minutos = document.querySelector(".minutos");
    const segundos = document.querySelector(".segundos");

    setInterval(() => {
        let hora = new Date();
        if (parseInt(hora.getHours()) < 10) {
            horas.innerHTML = `0${hora.getHours()}`;
        } else {
            horas.innerHTML = hora.getHours();
        }
        if (parseInt(hora.getMinutes()) < 10) {
            minutos.innerHTML = `0${hora.getMinutes()}`;
        } else {
            minutos.innerHTML = hora.getMinutes();
        }
        if (parseInt(hora.getSeconds()) < 10) {
            segundos.innerHTML = `0${hora.getSeconds()}`;
        } else {
            segundos.innerHTML = hora.getSeconds();
        }
        days.innerHTML = semana[hora.getDay() - 1];
    }, 1000);
}